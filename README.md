# Thanos

Destroy half of the files in the current directory.

## Usage

```console
thanos <option>
```

### Options
```console
thanos help
```
Display information about the program.

```console
thanos snap
```
Randomly deletes half of the files in the directory.

```console
thanos kill
```
Deletes one random file in the current directory.

## Compilation

Requires cargo to be build. Don't have it? Install [Rust!](https://www.rust-lang.org/learn/get-started)

```console
git clone https://gitlab.com/MaxPow2718r/thanos
cd thanos
cargo build --release
```

## Misc
- The program does removes the files. Use it on your own risk.
- Why? funny

## TODO

- [ ] Write docs
- [ ] Accept a path as an argument
