use std::{env, process};
use thanos::Cli;

fn main() {
    let args = Cli::get_opt(env::args()).unwrap_or_else(|err| {
        eprintln!("Couldn't run: {err}");
        thanos::usage();
        process::exit(1);
    });

    if let Err(e) = thanos::run(args) {
        eprintln!("Couldn't run: {}", e);
        thanos::usage();
        process::exit(1);
    }
}
