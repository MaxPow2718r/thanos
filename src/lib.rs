use rand::prelude::SliceRandom;
use std::error::Error;
// use std::fs;
use std::path::Path;
use walkdir::{DirEntry, WalkDir};

enum FileRet {
    Multiple(Vec<DirEntry>),
    Single(DirEntry),
    Error(&'static str),
}

pub struct Cli {
    pub option: String,
    // TODO: Add dir_path functionality
    // pub dir_path: String,
}

impl Cli {
    pub fn get_opt(mut args: impl Iterator<Item = String>) -> Result<Cli, &'static str> {
        args.next();

        let option = match args.next() {
            Some(args) => args,
            None => return Err("no option given"),
        };

        // TODO: Manage optional argument path
        Ok(Cli { option })
    }
}

pub fn run(args: Cli) -> Result<(), &'static str> {
    match args.option.as_str() {
        "help" => {
            usage();
        }
        "snap" => match get_rand_files(true) {
            FileRet::Multiple(files) => {
                for file in files {
                    println!("You have my respect: {}", file.path().display());
                    match rm_file(file.path()) {
                        Ok(()) => println!("I hope they remember you"),
                        Err(e) => {
                            eprintln!("{}", e);
                            return Err("Couldn't read the directory");
                        }
                    }
                }
            }
            FileRet::Error(e) => {
                eprintln!("{}", e);
                return Err("Couldn't read the directory");
            }
            _ => unreachable!(),
        },
        "kill" => match get_rand_files(false) {
            FileRet::Single(file) => {
                println!("I hope they remember you {}", file.path().display());
                match rm_file(file.path()) {
                    Ok(()) => println!("I hope they remember you"),
                    Err(e) => {
                        eprintln!("{}", e);
                        return Err("Couldn't read the directory");
                    }
                }
            }
            FileRet::Error(e) => {
                eprintln!("{}", e);
                return Err("Couldn't read the directory");
            }
            _ => unreachable!(),
        },
        _ => return Err("bad argument"),
    };

    Ok(())
}

pub fn usage() {
    println!("thanos: Deletes half of the files in the current directory");
    println!("\x1b[1mUSAGE\x1b[0m");
    println!("    thanos <option>");
    println!("\x1b[1mOPTIONS\x1b[0m");
    println!("    help -- displays this help message");
    println!("    snap -- deletes half of the files in the project");
    println!("    kill -- deletes one random file from the project");
    println!();
    println!();
    println!("\x1b[31;127m{}\x1b[0m", "WARNING");
    println!("This program WILL delete the files, theres not restore option");
}

////////////////////////////////////////////////////////////////////////////////
// Private functions
////////////////////////////////////////////////////////////////////////////////

fn rm_file(file: &Path) -> Result<(), Box<dyn Error>> {
    // fs::remove_file(file)?;
    println!("{}", file.display());
    Ok(())
}

fn get_rand_files(multiple: bool) -> FileRet {
    let mut rng = rand::thread_rng();

    // rust things
    let files: Vec<_> = WalkDir::new("./")
        .into_iter()
        .filter(|file| {
            file.as_ref()
                .ok()
                .expect("Problem with file")
                .metadata()
                .expect("File has no metadata")
                .is_file()
        })
        .collect();

    // I'm not handeling the error here. I skipped, but this was not intentional. Why this happend?
    // IDK.
    if multiple {
        let rand_items: Vec<_> = files
            .choose_multiple(&mut rng, files.len() / 2)
            .map(|file| file.as_ref().unwrap().clone())
            .collect();

        FileRet::Multiple(rand_items)
    } else {
        if let Some(rand_items) = files.choose(&mut rng) {
            match rand_items.clone() {
                Ok(file) => FileRet::Single(file.clone()),
                Err(_) => FileRet::Error("couldn't get a random file"),
            }
        } else {
            FileRet::Error("random choise failed, returned None")
        }
    }
}
